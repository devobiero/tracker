# Tracking Backend
## Description
Modelling for the interactions of a Rider, Battery, Vehicle and Station entities. Majority of the operations
are captured in [Battery class](/src/main/java/com/george/tracker/entity/Battery.java).

Operations implemented:
* Swap
* Charge
* Consume

To calculate distance travelled by a driver:
* Solution relies on [InfluxDB core](https://docs.influxdata.com/flux/v0.x/stdlib/experimental/geo/st_length/) to calculate the spherical distance

API endpoints:
* `GET /batteries/available` to list available batteries
* `POST /batteries` to register a battery
* `POST /events/in` to capture and process a timeseries event

## Architecture

![backend](./docs/architecture.png)
  
## Technologies Used
* Java 11
* Spring boot
* Postgres
* InfluxDB
* RabbitMQ
* Telegraf

## Getting started
### Requirements
* Docker version 17.06.0 or higher
* Docker compose version 1.14.0 or higher
* Java version 11 or higher

### Running the application
* Run `docker-compose up`, server should be available on http://localhost:8080/
* Collection of HTTP endpoints can be found [here](http/api.http)

### Local development
* Run `./mvnw clean install` to install required dependencies
* Run `./mvnw test` to run unit and integration tests

## Demo
![](./demo/recording.mov)
