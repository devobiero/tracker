package com.george.tracker;

import io.restassured.RestAssured;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = {TrackerApplication.class})
public class IntegrationTests {
  @Value("${local.server.port:0}")
  protected int port;
  @MockBean
  private RabbitAdmin rabbitAdmin;

  @BeforeEach
  void setUp() {
    RestAssured.port = port;
  }
}
