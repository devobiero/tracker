package com.george.tracker.entity;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

class BatteryTest {
  private Battery battery;
  private Motorcycle motorcycle;
  private Station station;

  @BeforeEach
  void setUp() {
    battery = Battery
        .builder()
        .type(Battery.BatteryType.NICKEL_CADMIUM)
        .build();
    motorcycle = Motorcycle
        .builder()
        .model("Boda")
        .build();
    station = Station
        .builder()
        .name("Location A")
        .build();
  }

  @Test
  void canChargeBattery() {
    battery.charge(100);
    assertEquals(100, battery.getChargeCapacity());
  }

  @Test
  void canSwapDepletedWithFullyChargedBattery() {
    Battery another = Battery.builder().build();
    another.charge(50);
    station.receive(another);

    battery.charge(100);
    battery.consume(40);
    Battery swapBattery = battery.swap(motorcycle, station);

    assertEquals(50, swapBattery.getChargeCapacity());
  }

  @Test
  void canCalculateChargeConsumed() {
    battery.charge(100);

    motorcycle.assign(battery);
    motorcycle.useCharge(10);
    motorcycle.useCharge(10);
    motorcycle.useCharge(20);

    assertEquals(60, motorcycle.getBattery().getCurrentCharge());
  }

  @Test
  void shouldThrowAnErrorIfAChargedBatteryCannotBeFound() throws Exception {
    try {
      Battery anotherBattery = Battery.builder().build();
      anotherBattery.charge(10);
      motorcycle.assign(battery);
      motorcycle.useCharge(10);
      anotherBattery.swap(motorcycle, station);
      fail();
    } catch (IllegalArgumentException e) {
      assertEquals("No batteries to be swapped", e.getMessage());
    }
  }

  @Test
  void canCalculateCostOfCharge() {
    battery.charge(100);
    motorcycle.assign(battery);
    motorcycle.useCharge(10);
    motorcycle.useCharge(10);
    motorcycle.useCharge(20);

    int cost = battery.costOfCharge(1);

    assertEquals(40, cost);
  }
}
