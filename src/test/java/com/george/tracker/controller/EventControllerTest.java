package com.george.tracker.controller;

import com.george.tracker.IntegrationTests;
import com.george.tracker.entity.Event;
import com.george.tracker.service.EventService;
import io.restassured.http.ContentType;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.mock.mockito.MockBean;

import static io.restassured.RestAssured.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

class EventControllerTest extends IntegrationTests {
  @MockBean
  private EventService eventService;

  @Test
  void canPublishEvent() {
    Event event = Event.
        builder()
        .serialNumber("1234")
        .latitude(1245d)
        .longitude(12345d)
        .build();

    given().
        contentType(ContentType.JSON).
        body(event).
        when().
        post("/events/in").
        then().
        statusCode(200);

    verify(eventService, times(1)).publish(event, "events/raw");
  }
}
