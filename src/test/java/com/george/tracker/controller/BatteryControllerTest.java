package com.george.tracker.controller;

import com.george.tracker.IntegrationTests;
import com.george.tracker.entity.Battery;
import io.restassured.http.ContentType;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.core.StringContains.containsString;

class BatteryControllerTest extends IntegrationTests {
  @Test
  void canFindAvailableBatteries() {
    given().
        contentType(ContentType.JSON).
        when().
        get("/batteries/available").
        then().
        statusCode(200).
        body(containsString("\"id\":200")).
        body(containsString("\"serialNumber\":\"12345\","));
  }

  @Test
  void canAddBattery() {
    Battery battery = Battery
        .builder()
        .chargeCapacity(100)
        .type(Battery.BatteryType.NICKEL_CADMIUM)
        .serialNumber("123456")
        .usedCharge(0)
        .build();

    given().
        contentType(ContentType.JSON).
        body(battery).
        when().
        post("/batteries").
        then().
        statusCode(201);
  }
}
