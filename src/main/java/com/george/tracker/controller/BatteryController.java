package com.george.tracker.controller;

import com.george.tracker.entity.Battery;
import com.george.tracker.service.BatteryService;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class BatteryController {
  private final BatteryService batteryService;

  public BatteryController(BatteryService batteryService) {
    this.batteryService = batteryService;
  }

  @GetMapping("/batteries/available")
  public List<Battery> listBatteries() {
    return batteryService.findAvailableBatteries();
  }

  @PostMapping("/batteries")
  @ResponseStatus(HttpStatus.CREATED)
  public Battery addBattery(@RequestBody BatteryDTO batteryDTO) {
    Battery battery = Battery
        .builder()
        .type(batteryDTO.getType())
        .serialNumber(batteryDTO.getSerialNumber())
        .chargeCapacity(batteryDTO.getCapacity())
        .build();
    return batteryService.addBattery(battery);
  }

  @Data
  @AllArgsConstructor
  @NoArgsConstructor
  private static class BatteryDTO {
    private String serialNumber;
    private Battery.BatteryType type;
    private int capacity;
  }
}
