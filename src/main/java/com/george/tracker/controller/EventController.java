package com.george.tracker.controller;

import com.george.tracker.entity.Event;
import com.george.tracker.service.EventService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/events")
public class EventController {
  private final EventService eventService;

  public EventController(EventService eventService) {
    this.eventService = eventService;
  }

  @PostMapping("/in")
  public void in(@RequestBody Event event) {
    eventService.publish(event, "events/raw");
  }
}
