package com.george.tracker.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.util.CollectionUtils;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
public class Station {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;
  private String name;
  private String location;

  @OneToMany(cascade = CascadeType.ALL)
  @JoinColumn(name = "station_id", referencedColumnName = "id")
  private List<Battery> batteries;

  public void receive(Battery battery) {
    if (CollectionUtils.isEmpty(this.batteries)) this.batteries = new ArrayList<>();
    this.batteries.add(battery);
  }

  /**
   * Receives a depleted battery and replaces it with a charged one.
   * Throws an error if a replacement cannot be found.
   */
  public Battery swap(Battery depleted) {
    receive(depleted);
    if (this.batteries.size() == 1) throw new IllegalArgumentException("No batteries to be swapped");
    return this.batteries
        .stream()
        .filter(Battery::isFullyCharged)
        .findFirst()
        .orElseThrow(() -> new IllegalArgumentException("A charged battery could not be found"));
  }
}
