package com.george.tracker.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@ToString(exclude = "motorcycle")
public class Battery implements Serializable {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;
  private BatteryType type;
  private String serialNumber;
  private int usedCharge;
  private LocalDateTime timeOfCharge;
  private int chargeCapacity;

  @OneToOne(mappedBy = "battery")
  @JsonIgnore
  private Motorcycle motorcycle;

  public void charge(int capacity) {
    this.setTimeOfCharge(LocalDateTime.now());
    this.setChargeCapacity(capacity);
  }

  public int getCurrentCharge() {
    return Math.abs(this.getChargeCapacity() - this.getUsedCharge());
  }

  public void consume(int unit) {
    this.usedCharge += unit;
  }

  /**
   * Swap a depleted with a fully charged battery
   */
  public Battery swap(Motorcycle motorcycle, Station station) {
    Battery chargedBattery = station.swap(this);
    motorcycle.assign(chargedBattery);
    return chargedBattery;
  }

  public boolean isFullyCharged() {
    return this.getChargeCapacity() == this.getCurrentCharge();
  }

  public int costOfCharge(int costPerCharge) {
    return this.getUsedCharge() * costPerCharge;
  }

  public enum BatteryType {
    LEAD_ACID,
    NICKEL_CADMIUM,
    POLYMER
  }
}
