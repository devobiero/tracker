package com.george.tracker.entity;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@ToString(exclude = "motorcycle")
public class Driver implements Serializable {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;
  private String firstName;
  private String lastName;
  private String phoneNumber;

  @OneToOne(mappedBy = "driver")
  private Motorcycle motorcycle;
}
