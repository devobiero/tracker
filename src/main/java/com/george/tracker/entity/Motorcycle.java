package com.george.tracker.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
public class Motorcycle implements Serializable {
  @Id
  @GeneratedValue
  private Long id;
  private String model;

  @OneToOne(cascade = CascadeType.ALL)
  @JoinColumn(name = "battery_id", referencedColumnName = "id")
  private Battery battery;

  @OneToOne(cascade = CascadeType.ALL)
  @JoinColumn(name = "driver_id", referencedColumnName = "id")
  private Driver driver;

  public void assign(Battery battery) {
    this.battery = battery;
  }

  /**
   * Consume a unit of charge
   */
  public void useCharge(int unit) {
    this.battery.consume(unit);
  }
}
