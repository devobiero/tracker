package com.george.tracker.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Event implements Serializable {
  private Long id;
  private String serialNumber;
  private double longitude;
  private double latitude;
  private Long driverId;
  private int consumed;
}
