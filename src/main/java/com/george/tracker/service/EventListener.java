package com.george.tracker.service;

import com.george.tracker.entity.Battery;
import com.george.tracker.entity.Driver;
import com.george.tracker.entity.Event;
import com.george.tracker.repository.BatteryRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class EventListener {
  private static final Logger logger = LoggerFactory.getLogger(EventListener.class);
  private final BatteryRepository batteryRepository;
  private final AmqpTemplate template;

  public EventListener(BatteryRepository batteryRepository, AmqpTemplate template) {
    this.batteryRepository = batteryRepository;
    this.template = template;
  }

  /**
   * Receive raw AMQP events, add tags/fields, extract charge usage and emit a rich event.
   * The Final event will be written to influxDB by telegraf agent
   */
  @RabbitListener(
      bindings = @QueueBinding(
          value = @Queue,
          exchange = @Exchange(value = "events/raw", type = ExchangeTypes.TOPIC),
          key = "*"))
  void process(Event event) {
    logger.info(String.format("Received an event from device: %s", event));
    Optional<Battery> optionalBattery = this.batteryRepository.findBySerialNumber(event.getSerialNumber());
    if (optionalBattery.isPresent()) {
      Battery battery = optionalBattery.get();
      battery.consume(event.getConsumed());
      this.batteryRepository.save(battery);

      Optional<Driver> driver = this.batteryRepository.findDriverByBattery(battery);
      if (driver.isPresent()) {
        event.setDriverId(driver.get().getId());
        template.convertAndSend("events/processed", event.getSerialNumber(), event);
      }
    }
  }
}
