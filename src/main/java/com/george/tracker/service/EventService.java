package com.george.tracker.service;

import com.george.tracker.entity.Event;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.stereotype.Service;

@Service
public class EventService {
  private final AmqpTemplate template;

  public EventService(AmqpTemplate template) {
    this.template = template;
  }

  /**
   * A helper method to publish events to different topics
   */
  public void publish(Event event, String topic) {
    template.convertAndSend(topic, event.getSerialNumber(), event);
  }
}
