package com.george.tracker.service;

import com.george.tracker.entity.Battery;
import com.george.tracker.repository.BatteryRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BatteryService {
  private static final Logger logger = LoggerFactory.getLogger(BatteryService.class);
  private final BatteryRepository batteryRepository;

  public BatteryService(BatteryRepository batteryRepository) {
    this.batteryRepository = batteryRepository;
  }

  /**
   * Fetch available batteries. This endpoint can take additional query
   * parameters (e.g station, battery type, charge status e.t.c)
   */
  public List<Battery> findAvailableBatteries() {
    logger.info("Fetching available batteries");
    return this.batteryRepository.findAvailableBatteries();
  }

  /**
   * Register a new org. wide battery. An org. wide battery is not attached to any station.
   * This endpoint can be extended to register a new battery to a specific station.
   */
  public Battery addBattery(Battery battery) {
    logger.info(String.format("Registering a new battery: %s", battery));
    return this.batteryRepository.save(battery);
  }
}
