package com.george.tracker.repository;

import com.george.tracker.entity.Battery;
import com.george.tracker.entity.Driver;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface BatteryRepository extends JpaRepository<Battery, Long> {
  @Query("select b from Battery b where b.usedCharge = 0")
  List<Battery> findAvailableBatteries();

  Optional<Battery> findBySerialNumber(String serialNumber);

  @Query("select d from Driver d join d.motorcycle m join m.battery b where b = ?1")
  Optional<Driver> findDriverByBattery(Battery battery);
}
